extern crate bh;
use bh::*;



fn main() {

    let body_data = vec![
        (-0.9f64, 0.2f64),
        (-0.5f64, -0.12f64),
        (0.7f64, 0.75f64),
        (-0.3f64, 0.26f64),
        (-0.1f64, 0.65f64),
        (0.1f64, -0.72f64),
        (0.3f64, 0.19f64),
        (0.51f64, 0.88f64),
        (0.7f64, -0.52f64),
        (0.9f64, -0.22f64)
    ];

    println!("{:?}\n", body_data);

    let mut tree = Node::as_bounds(Bounds {
        sw: Point { x: -1.0, y: -1.0 },
        ne: Point { x: 1.0, y: 1.0 }
    });

    let mut bodies = body_data
        .iter()
        .map(|val| Node::as_body( Point { x: val.0, y: val.1 }, 100.0 ))
        .collect::<Vec<Node>>();

    for _ in 0..body_data.len() {
        tree.insert(Box::new(bodies.remove(0)));
    }

    println!("{}", tree.display(0));    
}

