//! Contains the various types used in the Barnes-Hut tree.
use std::fmt;



/// Defines a 2-dimensional cartesian coordinate.
///
/// # Examples
/// 
/// Default initialization:
///
/// ```
/// use bh::Point;
///
/// let p = Point { x: 10.0, y: 1.0};
/// assert_eq!(1.0, p.y);
///
/// let q = p;
/// assert_eq!(10.0, q.x);
/// ```
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Point {
    /// The x-coordinate value 
    pub x: f64,
    /// The y-coordinate value 
    pub y: f64,
}



/// Defines a tree-node boundary.
///
/// The boundary is defined using its bottom-left (southwest) and top-right 
/// (northeast) points.
///
/// # Example:
///
/// ```
/// use bh::{Bounds, Point};
///
/// let sw = Point { x: 1.0, y: 2.0 };
/// let ne = Point { x: 3f64, y: 4f64 };
///
/// let bounds = Bounds { sw: sw, ne: ne };
/// 
/// assert_eq!(bounds.sw.y, 2f64);
/// assert_eq!(bounds.ne.x, 3.0);
/// ```
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Bounds {
    /// South-west point (bottom-left)
    pub sw: Point,
    /// North-east point (top-right)
    pub ne: Point,
}



/// Defines a single node 
/// 
/// This is the main unit used to build up the barnes-hut tree. 
#[derive(Clone, Debug, PartialEq)]
pub struct Node {
    /// Node boundary
    pub bounds: Bounds,
    /// Centre of mass
    pub centre: Point,
    /// Acuumulated mass
    pub mass: f64,
    /// List of children 
    pub children: Vec<Box<Node>>,
}
