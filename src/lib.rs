pub mod types;
pub use types::{Bounds, Node, Point};
use std::fmt;



/// Returns the centre of a `Bounds` instance
///
/// # Example 
///
/// ```
/// use bh::{Bounds, Point, get_centre};
///
/// let p = Point { x: 0.5, y: 0.5 };
/// let b = Bounds {
///     sw: Point { x: 0f64, y: 0f64 }, 
///     ne: Point { x: 1f64, y: 1f64 }
/// };
///
/// assert_eq!(get_centre(b).x, p.x);
/// assert_eq!(get_centre(b).y, 0.5);
/// ```
pub fn get_centre(bounds: Bounds) -> Point {
    Point {
        x: bounds.ne.x - ((bounds.ne.x - bounds.sw.x) / 2.0),
        y: bounds.ne.y - ((bounds.ne.y - bounds.sw.y) / 2.0),
    }
}



/// Determines whether a given `Point` instance exists within these 
/// `Bounds` instance limits.
///
/// The result **excludes** the bottom left values, and **includes** the top 
/// right ones.
///
/// # Arguments
/// * `bounds` - `Bounds` instance being tested
/// * `point` - `Point` instance being tested 
///
/// # Example 
///
/// ```
/// use bh::{Bounds, Point, has_point};
///
/// let bounds = Bounds {
///     sw: Point { x: 0f64, y: 0f64 },
///     ne: Point { x: 1f64, y: 1f64 },
/// };
/// let point_within = Point { x:1f64, y: 1f64 };
/// let point_without = Point { x: 0f64, y: 0f64 }; 
///
/// assert_eq!(has_point(bounds, point_within), true);
/// assert_eq!(has_point(bounds, point_without), false);
/// ```
pub fn has_point(bounds: Bounds, point: Point) -> bool {
    let contains_x = point.x <= bounds.ne.x && point.x > bounds.sw.x;
    let contains_y = point.y <= bounds.ne.y && point.y > bounds.sw.y;

    if contains_x && contains_y {
        return true
    } else {
        return false
    }
}



/// Subdivide the given `Bounds` instance into 4 child instances. 
/// The children are generated and returned in a `Vector` in the order:
///
/// * Top-left
/// * Top-right 
/// * Bottom-left 
/// * Bottom-right 
///
/// # Example 
///
/// ```
/// use bh::{Bounds, Point, subdivide};
///
/// let bounds = Bounds {
///     sw: Point { x: 0f64, y: 0f64 },
///     ne: Point { x: 1f64, y: 1f64 },
/// };
/// let child_bounds = subdivide(bounds);
/// 
/// assert_eq!(child_bounds.len(), 4);
/// assert_eq!(child_bounds[2].ne.y, 0.5);
/// ```
pub fn subdivide(bounds: Bounds) -> Vec<Bounds> {
    let centre = get_centre(bounds);

    return vec![
        Bounds {
            sw: Point { x: bounds.sw.x, y: centre.y },
            ne: Point { x: centre.x, y: bounds.ne.y }
        },
        Bounds {
            sw: Point { x: centre.x, y: centre.y },
            ne: Point { x: bounds.ne.x, y: bounds.ne.y }
        },
        Bounds {
            sw: Point { x: bounds.sw.x, y: bounds.sw.y },
            ne: Point { x: centre.x, y: centre.y }
        },
        Bounds {
            sw: Point { x: centre.x, y: bounds.sw.y },
            ne: Point { x: bounds.ne.x, y: centre.y }
        }
    ]
}




impl fmt::Display for Point {
    // Displays the `Point` instance as a string. The output is limited to 2 
    // decimal places by default. For example: `[2.45, 3.64]`
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{:.2}, {:.2}]", self.x, self.y)
    }
}



impl Node {
    /// Instantiates a new `Node` instance with defined boundaries, but no content.
    ///
    /// # Arguments 
    ///
    /// * `bounds` - The boundary values for the instance, defined as a `Bounds` instance
    ///
    /// # Example
    /// 
    /// ```
    /// use bh::{Bounds, Node, Point};
    ///
    /// let node = Node::as_bounds(Bounds {
    ///     sw: Point { x: 0f64, y: 0f64 },
    ///     ne: Point { x: 1f64, y: 1f64 }
    /// });
    ///
    /// assert_eq!(node.get_bounds().ne.y, 1.0);
    /// assert_eq!((Point { x: 0.5, y: 0.5 }).x, node.get_centre().x);
    /// assert_eq!(node.get_mass(), 0f64);
    /// assert_eq!(node.children.len(), 0);
    /// ```
    pub fn as_bounds(bounds: Bounds) -> Node {
        Node {
            bounds: bounds,
            centre: get_centre(bounds),
            mass: 0.0,
            children: vec![],
        }
    }

    /// Instantiates a new `Node` instance with body properties, but no bonudary.
    ///
    /// # Arguments 
    ///
    /// * `centre` - The centre of mass of the body 
    /// * `mass` - The mass of the body 
    ///
    /// # Example
    /// 
    /// ```
    /// use bh::{Node, Point};
    ///
    /// let node = Node::as_body(Point { x: 0.5, y: 0.7 }, 150f64);
    ///
    /// assert_eq!(node.get_bounds().ne.y, 0.0);
    /// assert_eq!(0.7, node.get_centre().y);
    /// assert_eq!(node.get_mass(), 150.0);
    /// assert_eq!(node.children.len(), 0);
    /// ```
    pub fn as_body(centre: Point, mass: f64) -> Node {
        Node {
            bounds: Bounds { 
                sw: Point { x: 0.0, y: 0.0 }, 
                ne: Point { x: 0.0, y: 0.0 }
            },
            centre: centre,
            mass: mass,
            children: vec![],
        }
    }

    /// Returns the boundary of the instance 
    pub fn get_bounds(&self) -> Bounds { self.bounds }

    /// Returns the centre of the instance 
    pub fn get_centre(&self) -> Point { self.centre }

    /// Returns the mass of the instance 
    pub fn get_mass(&self) -> f64 { self.mass }

    /// Inserts a body into this node (recursively)
    ///
    /// # Arguments
    /// 
    /// * `body` - The `Node` to insert into this one. This `Node` should have been instantiated 
    ///     using the `as_body` method.
    ///
    /// # Example 
    ///
    /// ```
    /// use bh::{Bounds, Node, Point};
    ///
    /// let body = Node::as_body(Point { x: 0.5, y: 0.5 }, 100f64);
    /// let mut tree = Node::as_bounds(Bounds {
    ///     sw: Point { x: 0.0, y: 0.0 },
    ///     ne: Point { x: 1.0, y: 1.0 }
    /// });
    ///
    /// tree.insert(Box::new(body));
    /// assert_eq!(tree.get_mass(), 100f64);
    /// ```
    pub fn insert(&mut self, body: Box<Node>) -> Result<i32, i32> {
        if has_point(self.bounds, body.centre) {
            if self.mass > 0.0 {
                // Node has point 

                if self.children.len() > 0 {
                    for child in &mut self.children {
                        child.insert(body.clone());
                    }
                } else {
                    let child_bounds = subdivide(self.bounds);
                    for n in 0..child_bounds.len() {
                        let mut child_node = Node::as_bounds(child_bounds[n]);
                        child_node.insert(body.clone());
                        child_node.insert(Box::new(Node::as_body(self.centre, self.mass)));
                        self.children.push(Box::new(child_node));
                    }
                }
                // Update parent node values 
                self.centre.x = ((self.mass * self.centre.x) + (body.mass * body.centre.x)) / (self.mass + body.mass);
                self.centre.y = ((self.mass * self.centre.y) + (body.mass * body.centre.y)) / (self.mass + body.mass);
                self.mass += body.mass;
            } else {
                // Empty node 
                self.mass = body.mass;
                self.centre = body.centre;
            }
        } else {
            return Err(1)
        }        
        Ok(0)
    }

    /// Returns a formatted string display of the tree 
    pub fn display(&self, level: i32) -> String {
        let mut tree = String::new();
        tree = tree + format!("{} : {}\n", self.mass, self.centre).as_str();

        if self.children.len() > 0 {
            let display_level = level + 1;
            let indent_level = (0..display_level).map(|_| "+ ").collect::<String>();

            for child_node in &self.children {
                tree = tree + indent_level.as_str() + child_node.display(display_level).as_str();
            }
        }
        tree 
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Mass: {:.2}, Centre: {}", self.mass, self.centre)
    }
}